from pydexcom import Dexcom
#dexcom = Dexcom("useraccount", "password","ous=True") # add ous=True if outside of US
dexcom = Dexcom("useraccount", "password") # add ous=True if outside of US
trends = ["","Rising Quickly","Rising","Rising Slightly","Steady","Falling Slightly","Falling","Falling Quickly","Unable to determine trend","Trend Unavailable"]
bg = dexcom.get_current_glucose_reading()
print("{")
print("\t\"status\": {")
print("\t\t\"SensorBGLmg\": \""+str(bg.value)+"\",")
print("\t\t\"SensorBGLmmol\": \""+str(bg.mmol_l)+"\",")
print("\t\t\"SensorDate\": \""+str(bg.time)+"\",")
print("\t\t\"BGLtrend\": \""+trends[bg.trend]+"\"")
print("\t\t}")
print("}")

