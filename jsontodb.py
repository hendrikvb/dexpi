# -*- coding:utf-8 -*-
import json
import sqlite3

JSON_FILE = "dexcom.json"
DB_FILE = "dexstatus.db"

jsondata = json.load(open(JSON_FILE))
conn = sqlite3.connect(DB_FILE)

SensorBGLmg = jsondata["status"]["SensorBGLmg"]
SensorBGLmmol = jsondata["status"]["SensorBGLmmol"]
SensorDate = jsondata["status"]["SensorDate"]
BGLtrend = jsondata["status"]["BGLtrend"]

data = (SensorBGLmg, SensorBGLmmol, BGLtrend)

c = conn.cursor()
c.execute('create table if not exists dexreadout (SensorBGLmg text, SensorBGLmmol text, SensorDate datetime, BGLtrend text)')
c.execute('insert into dexreadout values (?,?,datetime("now", "localtime"),?)', data)

conn.commit()
c.close()
