#!/bin/bash

# Cron script for the CNL24 project
# https://gitlab.com/hendrikvb/dexpi
# Use pydexcom Python tool to retrieve blood glucose readouts from the Dexcom portal
# Last Modified: 19 December 2021

export LC_NUMERIC="en_US.UTF-8"
export LC_TIME="en_US.UTF-8"

bottomalarm=79
# Set this to bottom alarm threshold

topalarm=300
# Set this to top alarm threshold

ghvolume=65
# Set Google Home Volume here

googlehome=192.168.2.121
googlehomenight=192.168.2.120
#googlehome=
#googlehomenight=
# Only needed when Google Home is used.
# Set value to IP for Google Home device.
# Use blank value to disable Google Home integration
# Define googlehomenight to use MP3 alarm at night
# Primary GH device is still used for standard notification (by default script behaviour)
# Set googlehomenight to same IP in case you want night-mode behaviour on primary device (audio warnings over GH device)

nighthour=20
morninghour=12
# Set window for *night mode* - Hours only - 24h format
# Example values:
# nighthour=20
# morninghour=7
# This defines a window: 20:00 to 7:59 for which an additional Google Home device is used

language=en
# Customize where needed above

path=/root/dexpi/

cd $path

date=`date`
chour=`date +"%H"`

python3 dexcomdump.py > dexcom.json 2> /dev/null
bgl=`cat dexcom.json | jq '.status.SensorBGLmg' | sed s/\"//g`
   
python jsontodb.py
python dbtojson.py
   
if [[ -z $bgl ]]; then
  echo "Zero Values - $date" > status.log
  if [[ ! -z $googlehome ]]; then
    python3 ghsay.py $googlehome "Sensor returning zero levels." $language $ghvolume &
  fi
  exit
fi;
   
if [[ $bgl == 0 ]]; then
  echo "Empty Values - $date" > status.log
  if [[ ! -z $googlehome ]]; then
    python3 ghsay.py $googlehome "Sensor returning blank levels." $language $ghvolume &
  fi
  exit
fi

# Normal reading
if [[ ! -z $googlehome ]]; then
  python3 ghsay.py $googlehome "Blood Glucose Level is $bgl" $language $ghvolume &
fi
