import sqlite3 as sql
import os
import csv
from sqlite3 import Error

try:

  # Connect to database
  conn=sql.connect('dexstatus.db')
  cursor = conn.cursor()
  cursor.execute("select * from dexreadout")
  with open("dexreadout.csv", "w") as csv_file:
      csv_writer = csv.writer(csv_file, delimiter=",", quoting=csv.QUOTE_ALL)
      csv_writer.writerow([i[0] for i in cursor.description])
      csv_writer.writerows(cursor)

except Error as e:
  print(e)

finally:
  conn.close()