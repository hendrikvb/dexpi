# dexpi
## A Raspberry Pi based project providing a simple Blood Glucose result dashboard, including graphs and data listing. Project relies on a DexCom Blood Glucose meter.

## DISCLAIMER (legal yada yada)
Using the information and files in this project at your own risk. Please **Do not use information including in this repository to make medical decisions!**

Customize the ```dex.sh``` script to your needs! This script is provided as-is and works for me. Out of the box installations do NOT send bolus information to your pump! Use at own risk!

## SUPPORT
As we recently switched Insuline Pump models, this specific project is still under development!

## Premise
Use a Raspberry Pi to provide a simple dashboard and increase the audible alarms for a Dexcom Glucose Meter when querying a connected insuline pump.

## References
A significant portion of the code and information is based on the cnl24pi project and by work done by Gage Benne. See https://gitlab.com/hendrikvb/cnl24pi and https://github.com/gagebenne/pydexcom .

## FAQ
* Q: My Google Home no longer (or rarely) plays the messages.
* A: This project uses the gTTS Python3 library, which in turn uses Google Translate to generate MP3s of status messages. Google recenty changed some code, which broke gTTS. This bug is fixed in newer releases of gTTS. Run ``` pip3 install gTTS gTTS-token --upgrade```. A new set of script was included to pre-generate the most-common (glucose blood level) messages. This should respond in quicker and less-internet-required reactions. Run ```./ghsaycache.sh``` (or localized variations) to generate the MP3 in the ```mp3cache``` folder.

* Q: So what about using Nightscout (http://www.nightscout.info/) instead?
* A: Please do if you can! My goal was to have a quick and cheap solution. Nightscout is great but I didn't have a spare cell phone lying around and I have no need to monitor all the time. Plus, the geek factor is an extra bonus.

* Q: So what about using the DexCom Follow App instead?
* A: Please do! My goal was to have a quick and cheap solution **on top of the Follow app**. Some additional monitoring is always handy!

* Q: Can I just download and run?
* A: NO! Some configuration is needed. All of it is done in scripts and requires limited technical knowledge. Steps are documented below.

* Q: Do I need a Google Home? Pushover notifications?
* A: No but it may help you. To disable these, updates to the scripts are needed?

* Q: Can I use this "on the go"?
* A: Short answer? No. Long answer: Yes. You will need to set up your Raspberry Pi to run on a local Wifi Access Point. This projects expects a working Wifi connection. If you use a Google Home, it will need to be on the same Wifi Access Point as the Raspberry Pi.

* Q: What are the Pushover Notifications?
* A: Pushover will send specific messages through your cell phone, similar to a text message. It supports multiple phone types and can be used on phone groups. Your phone does not need to be on the same Wifi as the Raspberry Pi to receive notifications. A 7 day trial can be activated to try the usefulness. A $5 one time charge is needed if you wish to keep receiving notifications. See [the Pushover site](https://pushover.net) for more information.

* Q: Can I use this project freely?
* A: YES! Always will be! Use it freely. Spread it freely. Adjust it to your needs!

* Q: How difficult is it to customize the scripts?
* A: All code is readable and can be adjusted quite easily, although some technical experience may be needed. A customized (localized) version of some scripts is included. The only difference is in the message language sent to Google Home. Customize the messages to your liking. Some variables are included and may need to be maintained.

## Requirements
- A Raspberry Pi (Work has been done on a model 3 but also works fine on a model Zero W).
- The Dexcom App configured with Sharing, as documented by https://github.com/gagebenne/pydexcom. Use the IOS Configuration Credentials, **not the follower credentials!**
- Initial configuration of the Raspberry Pi running and automatically connect to a stable Wi-Fi access point.
- Speakers if you want audible alarms
- An MP3 file of your choice, to be used as alarm
- Optional Google ChromeCast device (Google Home, Google Home Mini, ChromeCast dongle, Google  Nest Mini)
- Optional a [pushover.net](https://pushover.net) account and API setup.

## Details
See the **install/install_rpi.txt** document for the ```pip``` package installations:

Run:
```pip install cython
pip3 install requests astm PyCrypto crc16 python-dateutil
pip3 install pychromecast gtts
pip3 install pydexcom
```
We use ```dex.sh``` to wrap a timed loop (10 minutes) around dumping data and converting to JSON. Open the dex.sh file (or localized version) to adjust it to your circumstances.

In a separate session, start up a simple HTTP server, for instance ***python -m SimpleHTTPServer 8080***

### Install and run
```
git clone https://gitlab.com/hendrikvb/dexpi.git
cd dexpi
./webserver.sh
./dex.sh &
```

Use a localized version (dutch) is also included in this repository. Change filename where needed.

To report the glucose levels at fixed intervals despite normal levels, use Cron to schedule the cron script. Run ```crontab -e``` and add the following line (depending on the script location):

```*/30 * * * * /root/dexpi/dex_cron.sh >>/root/cron.txt 2>&1```

Adjust the cron path and the actual cron shell script (```dex_cron.sh```) where needed (including Google Home IP, thresholds etc).

The dex script uses 2 MP3 files to signal low and high threshold violations. Make sure to place these files in the same folder as the dex.sh script. Customizing the filename can be done by editing the dex.sh script.
Alarm MP3s are called ```alarm.mp3``` for the low threshold (Hypo) and ```alarmhigh.mp3``` for high threshold (Hyper).

### Configuration and Customization 
Specific values should be updated to meet your requirements.

```
export LC_NUMERIC="en_US.UTF-8"
export LC_TIME="en_US.UTF-8"

bottomalarm=79
# Set this to bottom alarm threshold

topalarm=300
# Set this to top alarm threshold

ghvolume=65
# Set Google Home Volume here

amixer set PCM 100%
# Set speaker volume here

normalinterval=150
# Normal wait period is 2.5 minutes (150 seconds)

shortinterval=90
# Short interval is 1.5 minutes (90 seconds)

alarmlow=alarm.mp3
alarmhigh=alarmhigh.mp3
# Tweak MP3 files here for alarms
# Change lines below to play MP3s locally or through Google Home
# Use ghplay.py for Google Home with MP3s
# Use ghsay.py for Google Home with Voice Notification

#googlehome=192.168.2.111
#googlehomenight=192.168.2.165
googlehome=
googlehomenight=
# Only needed when Google Home is used.
# Set value to IP for Google Home device.
# Use blank value to disable Google Home integration
# Define googlehomenight to use MP3 alarm at night
# Primary GH device is still used for standard notification (by default script behaviour)
# Set googlehomenight to same IP in case you want night-mode behaviour on primary device (audio warnings over GH device) 

nighthour=20
morninghour=12
# Set window for *night mode* - Hours only - 24h format
# Example values:
# nighthour=20
# morninghour=7
# This defines a window: 20:00 to 7:59 for which an additional Google Home device is used

language=en
# Default language for Google Home is english.
```

Some custom bracket notifcations are defined at the bottom of the script. The notification bracket for values between 100 and 150 is listed below. Add or remove where needed. 
Notifications of brackets will only be sent when bracket changes are detected.

```
   # 100 - 150
   if [[ $bgl -gt 100 && $bgl -lt 150 && $norepeat -ne 20 ]]; then
        if [[ ! -z $googlehome ]]; then
            python3 ghsay.py $googlehome "Blood Glucose Level is between 100 and 150! Level is $bgl"
            # Comment / Uncomment to use Google Home to play alarm MP3 or text notification
        fi
        norepeat=20
        sleep $normalinterval 
        continue;
   fi;
```

### Google Home

This script can send notifications through your Google ChromeCast device (Google Home, Google Home Mini, Google ChromeCast Dongle, Google). To enable this integration, set the ```googlehome``` variable above to the IP address of your device.
Most code for the Google Home integration has been copied from https://www.gioexperience.com/google-home-hack-send-voice-programmaticaly-with-python/ .

Change your voice language to your prefered state in ```ghsay.py```. Also, customize the ```dex.sh``` script on the specific Google Home lines to use different messages. The current volume level of the Google Home device will be retained and restored after the notifications sent by these scripts.
Define the volume to be used in all relevant script as ```ghvolume```. See ```dex.sh```, ```dex_cron.sh``` (and any localized version you may have).

### Notifications (Pushover)

Additional notifications (cell phone etc) is also included using [Pushover](https://pushover.net). A one time subscription fee to Pushover.net may be needed. Add a bash comment (```#```) in ```dex.sh``` where needed to disable Pushover support. 
Edit ```pushover.py``` to add the App Token and the User Key:

```
# Change lines below!
# Look up this information on https://pushover.net
app_token = "<APP TOKEN HERE!!>"
user_key = "<USER KEY HERE!!>"
```

![Pushover](pushover.png "Pushover.net")

### Starting on power-up

By default, this project will not start on powering up the Raspberry. To make it start on power-up, a few methods can be used. Typically, an init.d script contains specific commands to start or stop a service. A simple script below is used to start-up but NOT perform any stop actions:
```
#!/lib/init/init-d-script
### BEGIN INIT INFO
# Provides:          dexpi
# Required-Start:    $network $syslog $remote_fs
# Required-Stop:     $network $syslog $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      1
# Short-Description: dexpi boot script 
# Description:       dexpi 
#                    submission daemon
### END INIT INFO
cd /root/dexpi/
./webserver.sh
./dex.sh &
# Update the dex.sh script path to meet any localized versions, if needed
```

Copy these lines in ```/etc/init.d/dexpi```. Set the file permissions to 755 (```chmod 755 /etc/init.d/dexpi```). To map the bootscript to the boot sequence, create a symbolic link: ```ln -s /etc/init.d/dexpi /etc/rc3.d/S01dexpi```.

### Additional information

The webserver will serve:
- dex.json: most recent JSON formatted pump readout
- chart1h.json: Readout list of the last 1 hour
- chart3h.json: Readout list of the last 3 hours
- chart6h.json: Readout list of the last 6 hours
- chart12h.json: Readout list of the last 12 hours
- chart24h.json: Readout list of the last 24 hours 
- chart.html: Historical graph based on 25 last readouts
- output.txt: Raw dump of most recent pump readout
- output.log: Log messages

Included in this repository is also:
- dbtocsv.py: Convert sqlite3 database entries for Dexcom readings to CSV format
- ghsay.py: Python script to send text notifications through Google Home
- ghplay.py: Python script to send MP3 files through Google Home
- dex.nl.sh: A customized version of the Dexcom script for dutch language. Use this as a reference for localized versions.
- dex_cron_nl.sh: A customized version of the Dexcom scheduler script for dutch language. Use this as a referenced for localized versions.
- pushover.py: The Python script that handles pushover.net notifications.

The webserver will, by default, display a directory listing when connecting on port 8080. Use your **Raspberry Pi IP** to connect to, for instance, the chart information:
```http://192.168.0.1:8080/chart.html```

Sample chart.html view. Graph has gap suppression enabled. Same gaps are not listed in table (bottom, not fully visible).
![Chart Sample](chart.png "Chart Sample")
