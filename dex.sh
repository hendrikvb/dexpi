#!/bin/bash

# Main loop script for the DexPi project
# https://gitlab.com/hendrikvb/dexpi
# Use pydexcom Python tool to retrieve blood glucose readouts from the Dexcom portal
# Last Modified: 19 December 2021

export LC_NUMERIC="en_US.UTF-8"
export LC_TIME="en_US.UTF-8"

bottomalarm=79
# Set this to bottom alarm threshold

topalarm=300
# Set this to top alarm threshold

ghvolume=65
ghnightvolume=65
# Set Google Home Volume here

amixer set PCM 100%
# Set speaker volume here

normalinterval=150
# Normal wait period is 2.5 minutes (150 seconds)

shortinterval=90
# Short interval is 1.5 minutes (90 seconds)

alarmlow=alarm.mp3
alarmhigh=alarmhigh.mp3
# Tweak MP3 files here for alarms
# Change lines below to play MP3s locally or through Google Home
# Use ghplay.py for Google Home with MP3s
# Use ghsay.py for Google Home with Voice Notification

googlehome=192.168.2.121
googlehomenight=192.168.2.120
#googlehome=
#googlehomenight=
# Only needed when Google Home is used.
# Set value to IP for Google Home device.
# Use blank value to disable Google Home integration
# Define googlehomenight to use MP3 alarm at night
# Primary GH device is still used for standard notification (by default script behaviour)
# Set googlehomenight to same IP in case you want night-mode behaviour on primary device (audio warnings over GH device) 

nighthour=20
morninghour=12
# Set window for *night mode* - Hours only - 24h format
# Example values:
# nighthour=20
# morninghour=7
# This defines a window: 20:00 to 7:59 for which an additional Google Home device is used

language=en

# Customize where needed above

norepeat=0
# Flag used to prevent repetive notifications. Ignore
# Possible values tested:
# 1: No sensor connection
# 2: Sensor reading excessive (calibration)
# 3: No reading / zero values
# 10: BGL too high
# 11: BGL too low
# 12: Sensor initializing
# 13: Calibration needed
# 14: Sensor or calibration error
# 15: Sensor end of life
# 16: Sensor calibration pending
# 20: Custom bracket notifications

noflap=0
# Flag used to prevent flapping

zeroflag=1
# Counter to trigger action after maxzero counts
maxzero=3
# Execute zerocommand after maxzero counts
zerocommand=/sbin/reboot

echo Press Ctrl-C to break
for (( ; ; ))
do
   date=`date`
   chour=`date +"%H"`
   if [ $zeroflag == $maxzero ]; then
     echo "Repetitive ZERO levels. Attempting to remediate - $date" | tee -a output.log
     echo "Repetitive ZERO levels. Attempting to remediate - $date" > status.log
     `$zerocommand`
   fi
   
   python3 dexcomdump.py > dexcom.json 2> /dev/null
   bgl=`cat dexcom.json | jq '.status.SensorBGLmg' | sed s/\"//g`
   
   if [[ ! -n "$bgl" ]]; then 
     bgl=0
     echo Oops
   fi;

   if [[ $bgl == 0 ]]; then
     echo "Empty Values. Trying again in $shortinterval seconds" | tee -a output.log
     echo "Empty Values - $date" > status.log
     ((zeroflag=zeroflag+1))
     if [[ $norepeat -ne 3 ]]; then
         if [[ ! -z $googlehome ]]; then
          echo ""
           # python3 ghsay.py $googlehome "Sensor level is zero." $language $ghvolume &
         fi
     fi
     norepeat=3
     sleep $shortinterval 
     continue;
   fi;

   python jsontodb.py
   python dbtojson.py
   
   if [[ -z $bgl ]]; then
     echo "Zero Values. Trying again in $shortinterval seconds" | tee -a output.log
     echo "Zero Values - $date" > status.log
     ((zeroflag=zeroflag+1))
     if [[ $norepeat -ne 3 ]]; then
         if [[ ! -z $googlehome ]]; then
            # python3 ghsay.py $googlehome "Sensor returning zero levels." $language $ghvolume &
            echo ""
         fi
     fi
     norepeat=3
     sleep $shortinterval 
     continue;
   fi;
   
   if [[ $bgl == 0 ]]; then
     echo "Empty Values. Trying again in $shortinterval seconds" | tee -a output.log
     echo "Empty Values - $date" > status.log
     ((zeroflag=zeroflag+1))
     if [[ $norepeat -ne 3 ]]; then
         if [[ ! -z $googlehome ]]; then
           echo ""
           # python3 ghsay.py $googlehome "Sensor returning blank levels." $languaga $ghvolume &
         fi
     fi
     norepeat=3
     sleep $shortinterval 
     if [[ $zeroflag == $maxzero ]]; then
        echo "Excessively high ZERO values. Attempting to remediate" | tee -a output.log
        echo "Excessively high ZERO values. Attempting to remediate - $date" > status.log
     	`$zerocommand`
     fi
     continue;
   fi;

   if [[ $bgl -lt $bottomalarm ]]; then
     echo "ALARM! Blood Glucose Levels too low! $bgl - $date" | tee -a output.log
     echo "ALARM! Blood Glucose Levels too low! $bgl - $date" > status.log 
     if [[ $norepeat -ne 10 ]]; then
     	mpg123 $alarmlow &
        if [[ ! -z $googlehome ]]; then
            # Comment / Uncomment to play local MP3. Combination is possible
            python3 ghsay.py $googlehome "Blood Glucose Level is too low! Level is $bgl" $language $ghvolume &
            python3 pushover.py "Blood Glucose Level is too low! Level is $bgl." &
            # python3 ghplay.py $googlehome $alarmlow &
            # Comment / Uncomment to use Google Home to play alarm MP3 or text notification
        fi
         if [[ ! -z $googlehomenight && ($chour -ge $nighthour || $chour -le $morninghour) ]]; then
            # Comment / Uncomment to play local MP3. Combination is possible
            # python3 ghsay.py $googlehomenight "Bloed Glucose Niveau is te laag! Waarde is $bgl." $language $ghvolume &
            # python3 pushover.py "Bloed Glucose Niveau is te laag! Waarde is $bgl." &
            python3 ghplay.py $googlehomenight $alarmlow &
            # Comment / Uncomment to use Google Home to play alarm MP3 or text notification
         fi

        echo "Blood Glucose Level too low! - $date" > status.log
        norepeat=10
      fi
      norepeat=10
      sleep $normalinterval
      continue;
   fi;
   
   if [[ $bgl -gt $topalarm ]]; then
     echo "ALARM! Blood Glucose Level too high! $bgl - $date" | tee -a output.log
     echo "ALARM! Blood Glucose Level too high! $bgl - $date" > status.log
     if [[ $norepeat -ne 11 ]]; then
        # mpg123 $alarmhigh &
        if [[ ! -z $googlehome ]]; then
            # Comment / Uncomment to play local MP3. Combination is possible
            python3 ghsay.py $googlehome "Blood Glucose Level is too high! Level is $bgl" $language $ghvolume &
            python3 pushover.py "Blood Glucose Level is too high! Level is $bgl." &
            # python3 ghplay.py $googlehome $alarmhigh &
            # Comment / Uncomment to use Google Home to play alarm MP3 or text notification
        fi
        if [[ ! -z $googlehomenight && ($chour -ge $nighthour || $chour -le $morninghour) ]]; then
            # Comment / Uncomment to play local MP3. Combination is possible
            # python3 ghsay.py $googlehomenight "Blood Glucose Level is too high! Level is $bgl." $language $ghvolume &
            # python3 pushover.py "Blood Glucose Level is too high! Level is $bgl." &
            python3 ghplay.py $googlehomenight $alarmhigh &
            # Comment / Uncomment to use Google Home to play alarm MP3 or text notification
        fi

        echo "Blood Glucose Level too high! - $date" > status.log
     fi
     norepeat=11
     sleep $normalinterval
     continue;
   fi;

   echo "No Alarm - $bgl - $date" | tee -a output.log
   echo "OK - $date" > status.log
   
   # Sample bracket notifications below - Customize where needed

   # 100 - 150
   if [[ $bgl -gt 100 && $bgl -lt 150 && $norepeat -ne 20 ]]; then
        if [[ ! -z $googlehome ]]; then
            python3 ghsay.py $googlehome "Blood Glucose Level is $bgl" $language $ghvolume &
            # Comment / Uncomment to use Google Home to play alarm MP3 or text notification
        fi
        norepeat=20
        sleep $normalinterval 
        continue;
   fi;

   # 150 - 200
   if [[ $bgl -gt 150 && $bgl -lt 200 && $norepeat -ne 21 ]]; then
        if [[ ! -z $googlehome ]]; then
            python3 ghsay.py $googlehome "Blood Glucose Level is $bgl" $language $ghvolume &
            # Comment / Uncomment to use Google Home to play alarm MP3 or text notification
        fi
        norepeat=21
        sleep $normalinterval 
        continue;
   fi;

   # 200 - 250
   if [[ $bgl -gt 200 && $bgl -lt 250 && $norepeat -ne 22 ]]; then
        if [[ ! -z $googlehome ]]; then
            python3 ghsay.py $googlehome "Blood Glucose Level is $bgl" $language $ghvolume &
            # Comment / Uncomment to use Google Home to play alarm MP3 or text notification
        fi
        norepeat=22
        sleep $normalinterval 
        continue;
   fi;

   # 250 - 300
   if [[ $bgl -gt 250 && $bgl -lt 300 && $norepeat -ne 23 ]]; then
        if [[ ! -z $googlehome ]]; then
            python3 ghsay.py $googlehome "Blood Glucose Level is $bgl" $language $ghvolume &
            # Comment / Uncomment to use Google Home to play alarm MP3 or text notification
        fi
        norepeat=23
        sleep $normalinterval 
        continue;
   fi;

   # 300 - 350
   if [[ $bgl -gt 300 && $bgl -lt 350 && $norepeat -ne 24 ]]; then
        if [[ ! -z $googlehome ]]; then
            python3 ghsay.py $googlehome "Blood Glucose Level is $bgl" $language $ghvolume &
            # Comment / Uncomment to use Google Home to play alarm MP3 or text notification
        fi
        norepeat=24
        sleep $normalinterval
        continue;
   fi;

   # 350 - 400
   if [[ $bgl -gt 350 && $bgl -lt 400 && $norepeat -ne 25 ]]; then
        if [[ ! -z $googlehome ]]; then
            python3 ghsay.py $googlehome "Blood Glucose Level is $bgl" $language $ghvolume &
            # Comment / Uncomment to use Google Home to play alarm MP3 or text notification
        fi
        norepeat=25
        sleep $normalinterval
        continue;
   fi;

   sleep $normalinterval
done
